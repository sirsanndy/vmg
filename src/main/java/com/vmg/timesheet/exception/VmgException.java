package com.vmg.timesheet.exception;

public class VmgException extends RuntimeException{
    public VmgException(String error){
        super(error);
    }
}
