package com.vmg.timesheet.service;

import com.vmg.timesheet.dto.NotificationEmail;

public interface MailService {
    void sendMail(NotificationEmail notificationEmail);
}
