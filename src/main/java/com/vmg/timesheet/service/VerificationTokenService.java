package com.vmg.timesheet.service;

import com.vmg.timesheet.model.VerificationToken;

import java.util.Optional;

public interface VerificationTokenService {
    void save(VerificationToken verificationToken);
    Optional<VerificationToken> findByToken(String token);
}
