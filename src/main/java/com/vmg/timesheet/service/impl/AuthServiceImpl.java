package com.vmg.timesheet.service.impl;

import com.vmg.timesheet.constant.MailConstant;
import com.vmg.timesheet.dto.AuthenticationResponse;
import com.vmg.timesheet.dto.LoginRequest;
import com.vmg.timesheet.dto.RegisterRequest;
import com.vmg.timesheet.exception.VmgException;
import com.vmg.timesheet.dto.NotificationEmail;
import com.vmg.timesheet.model.User;
import com.vmg.timesheet.model.VerificationToken;
import com.vmg.timesheet.security.JwtProvider;
import com.vmg.timesheet.service.AuthService;
import com.vmg.timesheet.service.MailService;
import com.vmg.timesheet.service.UserService;
import com.vmg.timesheet.service.VerificationTokenService;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final PasswordEncoder passwordEncoder;
    private final UserService userService;
    private final VerificationTokenService verificationTokenService;
    private final MailService mailService;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;

    @Override
    @Transactional
    public void signup(RegisterRequest registerRequest) {
        User user = new User();
        user.setUsername(registerRequest.getUsername());
        user.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        user.setEmail(registerRequest.getEmail());
        user.setEnabled(false);

        userService.save(user);

        String token = generateVerificationToken(user);
        mailService.sendMail(new NotificationEmail(MailConstant.ACTIVATE_SUBJECT, user.getEmail(), MailConstant.ACTIVATE_MESSAGE + token));
    }

    @Override
    @Transactional
    public void verifyAccount(String token) {
        Optional<VerificationToken> verificationToken = verificationTokenService.findByToken(token);
        if(verificationToken.isPresent()){
            verificationToken.ifPresent(this::fetchUserAndEnable);
        } else {
            throw new VmgException("Invalid Token");
        }
    }

    @Override
    public AuthenticationResponse login(LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
                loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtProvider.generateToken(authentication);
        return new AuthenticationResponse(token, loginRequest.getUsername());
    }

    @Transactional
    public void fetchUserAndEnable(VerificationToken verificationToken) {
        String username = verificationToken.getUser().getUsername();
        User user = userService.findByUsername(username).orElseThrow(() -> new VmgException("User not found with name - " + username));
        user.setEnabled(true);
        userService.save(user);
    }

    private String generateVerificationToken(User user) {
        String token = UUID.randomUUID().toString();
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setToken(token);
        verificationToken.setUser(user);

        verificationTokenService.save(verificationToken);
        return token;
    }
}
