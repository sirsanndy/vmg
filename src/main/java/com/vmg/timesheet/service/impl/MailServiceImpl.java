package com.vmg.timesheet.service.impl;

import com.vmg.timesheet.constant.MailConstant;
import com.vmg.timesheet.exception.VmgException;
import com.vmg.timesheet.dto.NotificationEmail;
import com.vmg.timesheet.service.MailService;
import com.vmg.timesheet.util.MailContentBuilderService;
import lombok.AllArgsConstructor;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
@AllArgsConstructor
public class MailServiceImpl implements MailService {
    private final JavaMailSender javaMailSender;
    private final MailContentBuilderService mailContentBuilderService;

    @Override
    @Async
    public void sendMail(NotificationEmail notificationEmail) {
         MimeMessagePreparator messagePreparator = mimeMessage -> messageHelper(notificationEmail, mimeMessage);
         try {
             javaMailSender.send(messagePreparator);
         }catch (MailException e){
             throw new VmgException("Exception occured when sent the message :" + e.getMessage());
         }
    }

    private MimeMessageHelper messageHelper(NotificationEmail notificationEmail, MimeMessage mimeMessage){
        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
        try {
            messageHelper.setFrom(MailConstant.NO_REPLY_ADDRESS);
            messageHelper.setTo(notificationEmail.getRecipient());
            messageHelper.setSubject(notificationEmail.getSubject());
            mimeMessage.setText((mailContentBuilderService.build(notificationEmail.getBody())));
        } catch (MessagingException e){
            throw new VmgException("Exception occured when set mime message : " + e.getMessage());
        }
        return messageHelper;
    }
}
