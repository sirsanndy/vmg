package com.vmg.timesheet.service;

import com.vmg.timesheet.dto.AuthenticationResponse;
import com.vmg.timesheet.dto.LoginRequest;
import com.vmg.timesheet.dto.RegisterRequest;

public interface AuthService {
    void signup(RegisterRequest registerRequest);
    void verifyAccount(String token);
    AuthenticationResponse login(LoginRequest loginRequest);
}
