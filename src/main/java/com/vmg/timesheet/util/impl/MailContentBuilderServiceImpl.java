package com.vmg.timesheet.util.impl;

import com.vmg.timesheet.constant.MailConstant;
import com.vmg.timesheet.util.MailContentBuilderService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
@AllArgsConstructor
public class MailContentBuilderServiceImpl implements MailContentBuilderService {

    private final TemplateEngine templateEngine;

    @Override
    public String build(String message) {
        Context context = new Context();
        context.setVariable(MailConstant.MESSAGE, message);
        return templateEngine.process(MailConstant.MAIL_TEMPLATE, context);
    }
}
