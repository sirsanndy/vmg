package com.vmg.timesheet.util;

public interface MailContentBuilderService {
    String build(String message);
}
