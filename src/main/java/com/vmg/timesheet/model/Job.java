package com.vmg.timesheet.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@Table(name = "job")
@EqualsAndHashCode(callSuper = true)
public class Job extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "start_time")
    private Date startTime;

    @Column(name = "end_time")
    private Date endTime;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "job_detail_id", referencedColumnName = "id")
    private JobDetail jobDetail;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "overtime_id", referencedColumnName = "id")
    private Overtime overtime;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private User user;
}
