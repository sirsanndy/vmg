package com.vmg.timesheet.constant;

public final class MailConstant {
    public static final String MAIL_TEMPLATE = "mail_template";
    public static final String MESSAGE = "message";
    public static final String NO_REPLY_ADDRESS ="no-reply@vmg-it.com";
    public static final String ACTIVATE_SUBJECT = "Please activate your account";
    public static final String ACTIVATE_MESSAGE = "Thank you for signing up to VMG Timesheet." +
            "Please click on the below url to activate your account : " + "http://localhost:8080/api/auth/account/verification/";

    private MailConstant() {
        throw new IllegalAccessError("Mail Constant Class");
    }
}
