package com.vmg.timesheet.constant;

public class SecurityConstant {
    public static final String JKS = "JKS";
    public static final String AUTHORIZATION = "Authorization";
    public static final String BEARER = "Bearer ";
}
