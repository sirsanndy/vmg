package com.vmg.timesheet.security;

import com.vmg.timesheet.constant.SecurityConstant;
import com.vmg.timesheet.exception.VmgException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;

@Service
public class JwtProvider {

    @Value("${keystore.alias}")
    private String keystoreAlias;

    @Value("${keystore.pass}")
    private String keystorePass;

    private KeyStore keyStore;

    @PostConstruct
    public void init(){
        try {
            keyStore = KeyStore.getInstance(SecurityConstant.JKS);
            InputStream resourceAsStream = getClass().getResourceAsStream("/vmg.jks");
            keyStore.load(resourceAsStream, keystorePass.toCharArray());
        } catch (KeyStoreException| NoSuchAlgorithmException| IOException| CertificateException e){
            throw new VmgException("Exception occured while loading keystore");
        }
    }
    public String generateToken(Authentication authentication){
        User principal = (org.springframework.security.core.userdetails.User) authentication.getPrincipal();
        return Jwts.builder()
                .setSubject(principal.getUsername())
                .signWith(getPrivateKey())
                .compact();
    }

    private PrivateKey getPrivateKey() {
        try{
            return (PrivateKey) keyStore.getKey(keystoreAlias, keystorePass.toCharArray());
        } catch (KeyStoreException| NoSuchAlgorithmException| UnrecoverableKeyException e){
            throw new VmgException("Exception occured while retrieving public key from keystore");
        }
    }

    public boolean validateToken(String jwt){
        Jwts.parserBuilder().setSigningKey(getPublicKey()).build().parseClaimsJwt(jwt);
        return true;
    }

    private PublicKey getPublicKey() {
        try {
            return keyStore.getCertificate(keystoreAlias).getPublicKey();
        } catch (KeyStoreException e){
            throw new VmgException("Exception occured while retrieving public key from certificate");
        }
    }

    public String getUsernameFromJwt(String token){
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(getPublicKey())
                .build()
                .parseClaimsJwt(token)
                .getBody();
        return claims.getSubject();
    }
}
